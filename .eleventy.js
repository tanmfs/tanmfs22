const config = {
    // URL Related
    pathPrefix: "/",

    // Templating Engine
    templateFormats: [
        "md",
        "njk",
        "html"
    ],

    //markdownTemplateEngine: "njk",
    //htmlTemplateEngine: "njk",
    //dataTemplateEngine: "njk",

    // Directory Management
    passthroughFileCopy: true,
    dir: {
        input: "src",
        output: "_site",
    }
};


const sassWatch = require('./src/sass-watch');
const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");
const stringify = require('javascript-stringify').stringify;
const { DateTime } = require("luxon");

module.exports = function(eleventyConfig) {
    // Run only when 11ty is in watch mode.
    if (process.argv.includes('--watch')) {
        // Watch Sass directory for updates.
        // sassWatch('./src/sass/main.scss', './_site/css/main.css');
        sassWatch('./src/sass/main.scss', './src/assets/css/main.css');
        // Refresh the browser when there are updates in the Sass directory.
        eleventyConfig.addWatchTarget('./src/sass/');
    }

    // Add plugins
    eleventyConfig.addPlugin(eleventyNavigationPlugin);
//    eleventyConfig.addPlugin(eleventyGoogleFonts);

    // Directory Management
    eleventyConfig.addPassthroughCopy("./src/assets");

    // Layout Alias
    eleventyConfig.addLayoutAlias("page",      "layouts/page.njk");
    eleventyConfig.addLayoutAlias("page-2col", "layouts/page-2col.njk");
    eleventyConfig.addLayoutAlias("images",    "layouts/images.njk");

    // Filters
    eleventyConfig.addNunjucksFilter('htmlDateString', (dateObj) => {
        return DateTime.fromJSDate(dateObj, {zone: 'utc'}).toFormat('yyyy-LL-dd');
    });

    eleventyConfig.addFilter('console', function(value) {
        const output = stringify(value, null, "\t", { maxDepth: 3 });
        console.log(output);
        return '';
    });


    return config;
};