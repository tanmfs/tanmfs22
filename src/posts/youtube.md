---
title: YouTube Channel!
date: 2022-01-03
excerpt: Please visit our YouTube channel and watch our videos.
youtube: 9IpaJKB-wzo
---

We have during the pandemic era produced a number of videos of us playing recompositions of a Strauss waltz, called "W314Z". Please visit our [YouTube channel](https://www.youtube.com/channel/UC8_uqxOGZvxSRmEjKAuRl3g).