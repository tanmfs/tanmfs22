---
title: New website up!
date: 2022-01-28
excerpt: We have been working on a new website for sometime now. Please take a look around and see what we do!
image:
    src: /assets/img/newweb.png
    alt: New website
    box: true
---

We have made a brand new design of the website. We are still adding old as well as new content to it so 
please get back here on a regular basis to see our upcoming tour schedule, listen to our music, look at our videos etc.

#### Design & Code
Nippon Pippon, Fredric Bergström

#### Tech info

We made it as a "static website" (there are no more CMS!) to avoid having the need for a web developer constantly 
updating it with security updates like you need with f ex WordPress, Drupal etc. 
This is a simple but powerful setup based on the javascript tool "[11ty](https://www.11ty.dev)" and the services
[Netlify](https://www.netlify.com) and [Gitlab](https://gitlab.com) in collaboration. The design components are based on the css app framework [Bulma](https://bulma.io).
Content is added as markdown or json files.