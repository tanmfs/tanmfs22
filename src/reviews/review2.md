---
title: Perhaps the most interesting Swedish album of the year
author: Claes Olsson
publisher: Musikindustri.se
date: 2015-12-28
excerpt: Perhaps the most interesting Swedish album of the year!
---

Vestibulum ut venenatis risus. Vestibulum at rhoncus diam. Ut ac dui ante. Etiam aliquam bibendum ex eget faucibus. Maecenas ornare ipsum congue, scelerisque ante nec, condimentum dui. Mauris pellentesque, nulla non posuere egestas, ipsum sem sodales ligula, in semper ipsum diam nec mauris. Nam purus felis, ornare et nisl eu, vehicula tristique sem. Morbi eu lorem porta, mattis sapien ac, consequat leo. Duis scelerisque felis vitae ultrices condimentum. Donec malesuada vehicula elit sit amet commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Quisque vel dui eu justo cursus tempus tincidunt at velit.

Ut et ante elementum, cursus ex et, tempus mi. Vestibulum convallis leo sapien, eget ultricies nisi malesuada posuere. Nunc tincidunt risus metus, sed aliquam diam porttitor in. Curabitur sit amet risus eros. Suspendisse at lectus sed erat feugiat finibus eu ac turpis. Duis quis odio in lectus sollicitudin semper. Sed iaculis blandit lectus vitae eleifend. Duis finibus felis odio, vitae finibus risus vestibulum et. Suspendisse potenti. Etiam eu maximus lorem. Cras eget ligula id diam accumsan euismod. Maecenas semper ut tortor at interdum. Aenean dui mauris, dignissim sit amet quam id, rutrum vestibulum sapien. Aenean varius bibendum odio, eget aliquet nisi cursus id. Curabitur tincidunt quis purus luctus consequat. Sed nec mollis lorem, vel consectetur ante.