---
title: Bios
tag: page
eleventyNavigation:
    key: Bios
    order: 3
---

![Mattias Petersson](/assets/img/matt.jpg)<br><small>Photo: Björn Eriksson, Andreas Söderberg - ©Moment 22 Film, Video AB</small>

## Mattias Petersson

Mattias Petersson was born in 1972 on an island off the southeast coast of Sweden. His musical career began with piano studies but nowadays he works as a composer and plays electronic instruments. He works mostly within the realm of experimental electronic music and sound art, but he has also been involved in more pop-like projects as an arranger, composer, producer and musician.

#### More about Mattias Petersson

* [http://www.mattiaspetersson.com](http://www.mattiaspetersson.com)

---

![George Kentros](/assets/img/george.jpg)<br><small>Photo: Björn Eriksson, Andreas Söderberg - ©Moment 22 Film, Video AB</small>

## George Kentros

The experimental violinist George Kentros has performed as a chamber musician, 
soloist, and sometime actor across Europe as well as in the US, Central 
America, Japan, and Oceania, He has specialized in the newest of new art music, 
and has to date commissioned more than 200 works by composers from 21 countries. 

In 1999 he founded the Stockholm based avantgarde art club SEKT, which was a 
pioneer in the presentation of genre-crossing alternative art expressions, 
and has since expanded his practice towards works on thé border of sound art, 
collaborating closely with artists such as Kaffe Matthews and Vinyl -terror & -horror.



#### More about George Kentros

* [https://www.georgekentros.com](https://www.georgekentros.com)
* [http://www.swinepearl.com](http://www.swinepearl.com)

