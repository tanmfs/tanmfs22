---
title: Photos
layout: images.njk
tag: photos
eleventyNavigation:
    key: Photos
    order: 6
---

Here are some photos

{% for photo in collections.photos %}
- [{{ photo.data.title }}]({{ photo.url }})
{% endfor %}
