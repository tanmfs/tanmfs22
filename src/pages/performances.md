---
title: Selected performances
tag: page
eleventyNavigation:
  key: Selected performances
  order: 4
---

**Here is a selection of concerts we have done the last years:**

#### 2023

  - January: Univ. of York, Concert Hall; Akud, Birmingham (UK)
  - April: Folkets Hus, Årsta
  - August: Uppsala, Konstskådningsfestival
  - October/November: Honshu, Japan: Grisette, Shobi University, Otooto +

#### 2022

  - April: Fylkingen, Stockholm
  - July: De Nor, Antwerp
  - October: Konzerthaus, Klagenfurt


#### 2021

  - April: Geigerfest [(link)](https://youtu.be/qF2FjivFJ1w?t=3155) 
  - Augusti: Freqvenz Festival, Kiel

#### 2020

- Pandemic
- Youtube Concert [(link)](https://www.youtube.com/channel/UC8_uqxOGZvxSRmEjKAuRl3g)

#### 2019

- October: Tokyo, Grisette
- November: Osaka, Environment 0g
- Kobe, Big Apple
- RANKfest, tour with GALHMM to 7 spots in Sweden

#### 2018

- October: Tokyo, Okayama, Fukuoka
- Otooto, Grisette, Void+, Shobi University

#### 2017

- C3 Festival tour
- March: Bimhuis, Amsterdam,
- De Doelen, Rotterdam,
- Musikgebouw Eindhoven
- Oosterport Groningen
- May: Tallinn Music Week
- November: Sound of Stockholm

#### 2016

- May: Festival de Mayo, Guadalajara
- September: Christchurch Concert Hall
- Wonder Bar, Littleton
- Wellington Pyramid Club
- Audio Foundation, Auckland

#### 2015

- February:  Café OTO – London, UK
- May: Worm, Rotterdam
- December: Phill Niblock, Experimental Intermedia

#### 2014

- May: The Stone, New York, USA
- May: Classical Next – Musikverein – Wien, Austria
- May: House of Sweden – House of Sweden – Washington DC, USA
- May: Geigerfestival – Storan – Göteborg, Sweden
- September: Nordische Botschaften Felleshus – Berlin, Germany
- November: reBell with Qarin Wikström & Daniel Boyacioglu at Sound of Stockholm – Kulturhuset Stadsteatern – Stockholm, Sweden

#### 2013

- March: Sound of Mu – Oslo, Norway
- October: reBiber release concert at SEKT! – Fylkingen – Stockholm, Sweden
- October:  Magnet festival – Vara Konserthus – Vara, Sweden
- November:  reBiber – Moderna Bar – Fylkingen 80 years, Modern Museum Stockholm

#### 2012

- January: – House of Sweden – Washington DC, USA
- Scandinavia House – New York, USA
- March: Musikgemaket, Halmstad slott – Sweden
- April: Cinars 2012 – Monument National – Montréal, Canada
- May: COMAfest, Växjö
- June:  WABE – Berlin, Germany
- July: Norberg Festival

#### 2011

- May: Bergen Festspel
- September: Wunderbar, Nybrokajen 11
- November: Geigerfest, Göteborg

#### 2010

- September: Nordic Music Days, Copenhagen

#### 2009

- April: Inter art Malmö
- August: Malmö Summerfest

#### 2008

- September: World New Music Days/Gaida Festival, Vilnius
- October: Release there are no more four seasons, Kulturhuset, Stockholm

#### 2006-2007

- January: Art’s Birthday, Södra Teatern
- April: Geiger Fest
- October: GAS-festival
- July: Norberg Festival

#### 2005

- March: La Primavera en la Habana, Museo Nacional de Bellas Artes – Havana, Cuba
- March: Ugglan, Stockholm
- Kalv Festival
- October: GAS-festival

#### 2004

- December: EMS 40 years celebration, Modern Museum, Stockholm
